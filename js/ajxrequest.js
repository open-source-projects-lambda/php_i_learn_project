function addStu(){
    let stuname = $("#stuname").val();
    let stuemail = $("#stuemail").val();
    let stupass = $("#stupass").val();
    console.log(stuname);
    console.log(stuemail);
    console.log(stupass);
    $.ajax({
        url:'Student/addstudent.php',
        method: "POST",
        data:{
            stuname : stuname,
            stuemail: stuemail,
            stupass: stupass,
            pageid:"studentadd",
        },
        success: function(data){
            console.log(data);
            if (data == "Successfully saved the data"){
                $("#successMsg").html('<span>Registration success !</span>')
            }else{
                $("#successMsg").html('<span>Please insert data correctly</span>')
            }
            $("#suform")[0].reset();
        },

    });
};

function logStu(){
    let stuemail=$("#stuLogemail").val();
    let password=$("#stuLogpass").val();
    console.log(stuemail);
    console.log(password);
    $.ajax({
        url: 'Student/loginstudent.php',
        method: "POST",
        data:{
            stuemail: stuemail,
            password: password,
            pageid:"studentlog",
        },
        success: function(data){
            console.log(data);
            $("#stuLoginForm")[0].reset();
        },
    });
};