<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Experimental project</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
<!-- start navigation -->
<nav class="navbar navbar-expand-sm  pl-5 fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php">iLearn</a>
    <span class="navbar-text">Learn and Implement</span>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav custom-nav pl-5">
        <li class="nav-item custom-nav-item"><a href="#" class="nav-link">Home</a></li>
        <li class="nav-item custom-nav-item"><a href="maincourses.php" class="nav-link">Courses</a></li>
        <li class="nav-item custom-nav-item"><a href="paymentstatus.php" class="nav-link">Payment</a></li>
        <li class="nav-item custom-nav-item"><a href="#" class="nav-link">My Profile</a></li>
        <li class="nav-item custom-nav-item"><a href="#" class="nav-link">Logout</a></li>
        <li class="nav-item custom-nav-item" data-bs-toggle="modal" data-bs-target="#stuLoginModalCenter"><a href="#" class="nav-link">Login</a></li>
        <li class="nav-item custom-nav-item" data-bs-toggle="modal" data-bs-target="#stuRegModalCenter"><a href="#" class="nav-link">Signup</a></li>
        <li class="nav-item custom-nav-item"><a href="#" class="nav-link">feedback</a></li>
        <li class="nav-item custom-nav-item" ><a href="#" class="nav-link">Contact</a></li>
        <li class="nav-item custom-nav-item" ><a href="coursedetails.php" class="nav-link">Details</a></li>
      </ul>
    </div>
  </div>
</nav>