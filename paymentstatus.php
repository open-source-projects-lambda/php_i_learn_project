<?php
    include('maininclude/header.php');
?>
<div class="container-fluid db-dark">
    <div class="row">
    <div class="container-fluid remove-vid-marg">
    <div class="vid-parent">
        <div class="vid-overlay"></div><video style="height:700px; width:100%; object-fit:cover;"  playsinline autoplay muted loop><source src="video/payment.mp4" ></video>
        <div class="vid-overlay"></div>
    </div>
    </div>
</div>

<div class="container">
    <h2 class="text-center my-4">Payment status</h2>
    <form action="" method="post">
        <div class="form-group row">
            <lable class="offset-sm-3 col-form-lable">Order ID: </lable>
            <div>
                <input type="text" name="" id="" class="form-control mx-3">
            </div>
            <div>
                <input type="submit" value="View" class="btn btn-primary mx-4">
            </div>
        </div>
    </form>
</div>

<?php
include('contact.php')
?>
<?php
    include('maininclude/footer.php');
?>