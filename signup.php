<!-- Vertically centered modal -->

<!-- Modal -->
<div class="modal fade" id="stuRegModalCenter" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">New Registration</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form  method="POST" id="suform">
            <div class="form-group ">
                <i class="fas fa-user"></i><label for="stuname" class="pl-2 font-weight-bold">Name</label>
                <input type="text" class="form-control" placeholder="Name" name="stuname" id="stuname">
            </div > 
            <div class="form-group">
                <i class="fas fa-envelope"></i><label for="stuemail" class="pl-2 font-weight-bold">Email</label>
                <input type="text" class="form-control" placeholder="Email" name="stuemail" id="stuemail">
                <small class="form-text">We'll never share your email</small>
            </div>
            </div>
            <div class="from-group">
            <i class="fas fa-key"></i><label for="stupass" class="pl-2 font-weight-bold">Password</label>
                <input type="password" class="form-control" placeholder="Password" name="stupass" id="stupass">
            </div>
        </form>
        <!-- End student registration modal -->
        <div class="modal-footer">
          <span id="successMsg"></span>
        <button type="button" class="btn btn-primary" id="#stsignup" onclick="addStu()">Signup</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> 
      </div>
      </div>
    </div>
  </div>
</div>