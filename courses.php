<div class="container mt-5">
    <h1 class="text-center"></h1>
    <!--  popular course card deck1-->
    <div class="card-deck mt-4">
        <a href="#" class="btn" style="text-align: left; padding: 0px; margin: 0px;">
        <div class="card">
            <img src="#" alt="" class="card-img-top" alt="Guitar">
            <div class="card-body">
                <h5 class="card-title">Learn Guitar Easy way</h5>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi qui veritatis sed vel ipsam neque quod pariatur illo aperiam quos ratione, voluptatem quasi, ipsa minus vero dolor voluptas id fuga?</p>
            </div>
            <div class="card-footer">
                <p class="card-text d-inline">Price: <small><del>&#8377 2000</del></small><span class="font-weight-bolder">&#8377 200</span></p><a href="coursedetails.php" class="btn btn-primary font-weight-bolder float-right">Enroll</a>
            </div>
          
        </div>
    </a>
    </div>
     <!--  popular course card deck2-->
     <div class="card-deck mt-4">
        <a href="#" class="btn" style="text-align: left; padding: 0px; margin: 0px;">
        <div class="card">
            <img src="#" alt="" class="card-img-top" alt="Guitar">
            <div class="card-body">
                <h5 class="card-title">Learn Guitar Easy way</h5>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi qui veritatis sed vel ipsam neque quod pariatur illo aperiam quos ratione, voluptatem quasi, ipsa minus vero dolor voluptas id fuga?</p>
            </div>
            <div class="card-footer">
                <p class="card-text d-inline">Price: <small><del>&#8377 2000</del></small><span class="font-weight-bolder">&#8377 200</span></p><a href="coursedetails.php" class="btn btn-primary font-weight-bolder float-right">Enroll</a>
            </div>
        </div>
    </a>
    </div>
     <!--  popular course card deck 3-->
     <div class="card-deck mt-4">
        <a href="#" class="btn" style="text-align: left; padding: 0px; margin: 0px;">
        <div class="card">
            <img src="#" alt="" class="card-img-top" alt="Guitar">
            <div class="card-body">
                <h5 class="card-title">Learn Guitar Easy way</h5>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi qui veritatis sed vel ipsam neque quod pariatur illo aperiam quos ratione, voluptatem quasi, ipsa minus vero dolor voluptas id fuga?</p>
            </div>
            <div class="card-footer">
                <p class="card-text d-inline">Price: <small><del>&#8377 2000</del></small><span class="font-weight-bolder">&#8377 200</span></p><a href="coursedetails.php" class="btn btn-primary font-weight-bolder float-right">Enroll</a>
            </div>
        </div>
    </a>
    </div>
     <!--popular course card deck 3-->
     <div class="card-deck mt-4">
        <a href="#" class="btn" style="text-align: left; padding: 0px; margin: 0px;">
        <div class="card">
            <img src="#" alt="" class="card-img-top" alt="Guitar">
            <div class="card-body">
                <h5 class="card-title">Learn Guitar Easy way</h5>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi qui veritatis sed vel ipsam neque quod pariatur illo aperiam quos ratione, voluptatem quasi, ipsa minus vero dolor voluptas id fuga?</p>
            </div>
            <div class="card-footer">
                <p class="card-text d-inline">Price: <small><del>&#8377 2000</del></small><span class="font-weight-bolder">&#8377 200</span></p><a href="" class="btn btn-primary font-weight-bolder float-right">Enroll</a>
            </div>
        </div>
    </a>
    </div>
</div>
<div class="text-center m2">
        <a href="" class="btn btn-danger btn-sm">View All courses</a>
</div>