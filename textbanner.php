<div class="container-fluid bg-danger txt-banner">
    <div class="row bottom-banner">
        <div class="col-sm">
            <h5><i class='fas fa-users mr-3'></i>Expert instructors</h5>
        </div>
        <div class="col-sm">
            <h5><i class='fas fa-book-open mr-3'></i>100+ coursers</h5>
        </div>
        <div class="col-sm">
            <h5><i class='fas fa-keyboard mr-3'></i>Lifetime Access</h5>
        </div>
        <div class="col-sm">
            <h5><i class='fas fa-rupee-sign mr-3'></i>Low Costs</h5>
        </div>
    </div>
</div>