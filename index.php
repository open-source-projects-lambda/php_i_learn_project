<!-- header section-->
<?php
include('maininclude/header.php');
?>
<!-- end header section-->
<!--  start video background-->
<?php
include('backgroundmedia.php');
?>
<!--  end video background-->

<!--  Start text banner-->
<?php
include('textbanner.php');
?>
<!--  end text banner-->

<!--  popular course-->
<?php
include('courses.php');
?>
<!-- end popular course-->

<!-- start contact us -->
<?php
include('./contact.php')
?>
<!-- end contact us-->
<!-- Start student registration modal -->
<?php
include('signup.php')
?>
<!-- End student registration modal -->
<!-- student login Modal -->
<?php
include("login.php");
?>
<!-- End login Modal -->
<!-- admin login Modal -->

<!-- Modal -->
<?php
include('admin.php');
?>
<!--include footer-->
<?php
include('maininclude/footer.php');
?>
