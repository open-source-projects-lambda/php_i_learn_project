<div class="container-fluid remove-vid-marg">
    <div class="vid-parent">
        <div class="vid-overlay"></div><video playsinline autoplay muted loop><source src="video/backgroundplay.mp4" ></video>
        <div class="vid-overlay"></div>
    </div>
    <div class="vid-content">
        <h1 class="my-content">Welcome to ISchool</h1>
        <small class="my-content">Learn and Implement</small> <br>
        <a href="" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#stuRegModalCenter">Get started</a>
        <!-- Button trigger modal -->
    </div>
    
</div>